import express from 'express';
import cors from 'cors';
import _ from 'lodash';
import bodyParser from 'body-parser';
import fetch from 'isomorphic-fetch';

const app = express();
app.use(bodyParser.json());
app.use(cors());
// app.use(isAdmin);

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
  .then(async (res) => {
    pc = await res.json();
  })
  .catch(err => {
    console.log('Чтото пошло не так:', err);
  });

function getItem(item) {
  if (item === undefined) {
    return null;
  }
  if (item == null) {
    return 'null';
  }
  if (typeof (item) === 'object') {
    return item;
  }
  if (typeof (item) === 'number') {
    return item.toString();
  }
  return `"${item.toString()}"`;
}

app.get('/task3A/', (req, res) => {
  res.send(pc);
});

app.get('/task3A/volumes', (req, res) => {
  let volumes = {};
  pc.hdd.forEach(function sumHddSize(hdd) {
    if (volumes[hdd.volume]) {
      volumes[hdd.volume] = volumes[hdd.volume] + hdd.size;
    } else {
      volumes[hdd.volume] = hdd.size;
    }
  });
  for(let item in volumes){
    volumes[item] += 'B';
  }
  res.json(volumes);
});

app.get('/task3A/:pathL1/', (req, res) => {
  const val = getItem(pc[req.params.pathL1]);
  if (val == null) {
    return res.status(404).send('Not Found');
  }
  return res.send(val);
});

app.get('/task3A/:pathL1/:pathL2/', (req, res) => {
  let val = getItem(pc[req.params.pathL1]);
  if ((typeof (val) === 'object') && (val != null)) {
    val = getItem(val[req.params.pathL2]);
    if (val == null) {
      return res.status(404).send('Not Found');
    }
    return res.send(val);
  }
  return res.status(404).send('Not Found');
});

app.get('/task3A/:pathL1/:pathL2/:pathL3/', (req, res) => {
  let val = getItem(pc[req.params.pathL1]);
  if ((typeof (val) === 'object') && (val != null)) {
    val = getItem(val[req.params.pathL2]);
    if ((typeof (val) === 'object') && (val != null)) {
      val = getItem(val[req.params.pathL3]);
      if (val == null) {
        return res.status(404).send('Not Found');
      }
      return res.send(val);
    }
  }
  return res.status(404).send('Not Found');
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
